<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GladeController extends Controller
{
    public function randomData(){
        $data=[
            'name'=>'Timothy Soladoye',
            'email'=>'tim.soladoye@gmail.com',
            'github'=>'timoye',
        ];
        return response()->json($data, 200);
    }
}
