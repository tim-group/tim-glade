<?php

namespace Tests\Unit;

//use PHPUnit\Framework\TestCase;
use Tests\TestCase;

class GladeTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testNotFound()
    {
        $response = $this->get('/user');
        $response->assertStatus(404);
    }

    public function testAPINotFound(){
        $response = $this->get('/api/random');
        $response->assertStatus(404)->assertExactJson([
            'status'=> 'fail',
            'details'=> 'Not found'
        ]);
    }

    public function testRandomData(){
        $response = $this->get('/api/random-data');
        $response->assertStatus(200)->assertJson([
            'name' => true,
            'github' => true,
        ]);
    }

}
