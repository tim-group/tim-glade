<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::get('/random-data', 'GladeController@randomData')->name('random.data');

Route::fallback(function (){
    $payload = [
        'status' =>'fail',
        'details' => 'Not found'
    ];
    return response()->json($payload, 404);
});
